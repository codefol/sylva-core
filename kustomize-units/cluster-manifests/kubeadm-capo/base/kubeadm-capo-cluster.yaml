apiVersion: cluster.x-k8s.io/v1beta1
kind: Cluster
metadata:
  name: ${CLUSTER_NAME}
  namespace: default
spec:
  clusterNetwork:
    services:
      cidrBlocks: ["10.43.0.0/16"]
    pods:
      cidrBlocks: ["10.42.0.0/16"]
    serviceDomain: cluster.local
  controlPlaneRef:
    apiVersion: controlplane.cluster.x-k8s.io/v1beta1
    kind: KubeadmControlPlane
    name: ${CLUSTER_NAME}-control-plane
  infrastructureRef:
    apiVersion: infrastructure.cluster.x-k8s.io/v1alpha5
    kind: OpenStackCluster
    name: ${CLUSTER_NAME}
---
apiVersion: controlplane.cluster.x-k8s.io/v1beta1
kind: KubeadmControlPlane
metadata:
  name: ${CLUSTER_NAME}-control-plane
  namespace: default
spec:
  kubeadmConfigSpec:
    preKubeadmCommands:
      - echo "fs.inotify.max_user_watches = 524288" >> /etc/sysctl.conf
      - echo "fs.inotify.max_user_instances = 512" >> /etc/sysctl.conf
      - sysctl --system
      # Remove default mirroring configuration for k8s.gcr.io as it can't coexist with registry config dir
      - sed -i '/k8s.gcr.io/d' /etc/containerd/config.toml
      - grep -q config_path /etc/containerd/config.toml || echo "[plugins.\"io.containerd.grpc.v1.cri\".registry]\n  config_path = \"/etc/containerd/registry.d\"" >>  /etc/containerd/config.toml
      - systemctl restart containerd.service
    files:
    - path: /etc/kubernetes/manifests/kube-vip.yaml
      owner: root:root
      permissions: "0644"
      content: |
        apiVersion: v1
        kind: Pod
        metadata:
          creationTimestamp: null
          name: kube-vip
          namespace: kube-system
        spec:
          containers:
          - args:
            - manager
            env:
            - name: svc_enable
              value: "true"
            - name: vip_arp
              value: "true"
            - name: port
              value: "6443"
            - name: vip_cidr
              value: "32"
            - name: cp_enable
              value: "true"
            - name: cp_namespace
              value: kube-system
            - name: vip_ddns
              value: "false"
            - name: vip_leaderelection
              value: "true"
            - name: vip_leaseduration
              value: "5"
            - name: vip_renewdeadline
              value: "3"
            - name: vip_retryperiod
              value: "1"
            - name: address
              value: ${CLUSTER_EXTERNAL_IP}
            - name: prometheus_server
              value: :2112
            # Renovate Bot needs additional information to detect the kube-vip version:
            # renovate: registryUrl=https://ghcr.io image=kube-vip/kube-vip
            image: ghcr.io/kube-vip/kube-vip:v0.5.11
            imagePullPolicy: Always
            name: kube-vip
            resources: {}
            securityContext:
              capabilities:
                add:
                - NET_ADMIN
                - NET_RAW
            volumeMounts:
            - mountPath: /etc/kubernetes/admin.conf
              name: kubeconfig
          hostAliases:
          - hostnames:
            - kubernetes
            ip: 127.0.0.1
          hostNetwork: true
          volumes:
          - hostPath:
              path: /etc/kubernetes/admin.conf
            name: kubeconfig
    postKubeadmCommands:
    - export KUBECONFIG=/etc/kubernetes/admin.conf
    - kubectl taint nodes --all node-role.kubernetes.io/master-
    initConfiguration:
      nodeRegistration:
        kubeletExtraArgs:
          provider-id: openstack:///{{ ds.meta_data.uuid }}
        name: '{{ ds.meta_data.name }}'
    joinConfiguration:
      nodeRegistration:
        kubeletExtraArgs:
          provider-id: openstack:///{{ ds.meta_data.uuid }}
        name: '{{ ds.meta_data.name }}'
  machineTemplate:
    metadata:
      labels:
        health-check: control-plane
    infrastructureRef:
      apiVersion: infrastructure.cluster.x-k8s.io/v1alpha5
      kind: OpenStackMachineTemplate
      name: ${CLUSTER_NAME}-control-plane
  replicas: ${CONTROL_PLANE_REPLICAS:=3}
  version: ${K8S_VERSION}
---
apiVersion: bootstrap.cluster.x-k8s.io/v1beta1
kind: KubeadmConfigTemplate
metadata:
  name: ${CLUSTER_NAME}-worker
  namespace: default
spec:
  template:
    spec:
      files: []
      joinConfiguration:
        nodeRegistration:
          kubeletExtraArgs:
            provider-id: 'openstack:///{{ ds.meta_data.uuid }}'
          name: '{{ ds.meta_data.name }}'
      preKubeadmCommands:
        - echo "fs.inotify.max_user_watches = 524288" >> /etc/sysctl.conf
        - echo "fs.inotify.max_user_instances = 512" >> /etc/sysctl.conf
        - sysctl --system
        # Remove default mirroring configuration for k8s.gcr.io as it can't coexist with registry config dir
        - sed -i '/k8s.gcr.io/d' /etc/containerd/config.toml
        - grep -q config_path /etc/containerd/config.toml || echo "[plugins.\"io.containerd.grpc.v1.cri\".registry]\n  config_path = \"/etc/containerd/registry.d\"" >>  /etc/containerd/config.toml
        - systemctl restart containerd.service
---
apiVersion: cluster.x-k8s.io/v1beta1
kind: MachineDeployment
metadata:
  name: ${CLUSTER_NAME}-worker
  namespace: default
spec:
  clusterName: ${CLUSTER_NAME}
  replicas: ${WORKER_REPLICAS:=0}
  selector:
    matchLabels: null
  template:
    spec:
      bootstrap:
        configRef:
          apiVersion: bootstrap.cluster.x-k8s.io/v1beta1
          kind: KubeadmConfigTemplate
          name: ${CLUSTER_NAME}-worker
      clusterName: ${CLUSTER_NAME}
      failureDomain: ${WORKER_AZ}
      infrastructureRef:
        apiVersion: infrastructure.cluster.x-k8s.io/v1alpha5
        kind: OpenStackMachineTemplate
        name: ${CLUSTER_NAME}-worker
      version: ${K8S_VERSION}
